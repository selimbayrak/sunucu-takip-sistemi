﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Mail;

namespace WcfService
{
    // NOT: "IService1" arabirim adını kodda ve yapılandırma dosyasında birlikte değiştirmek için "Yeniden Düzenle" menüsündeki "Yeniden Adlandır" komutunu kullanabilirsiniz.
    [ServiceContract]
    public interface IService1
    {
        //Konfigürasyon ayarları
        [OperationContract]
        List<TableConf> GetConf();
        [OperationContract]
        void PutConf(TableConf obj);

        //server alma ve kaydetme fonksiyonları
        [OperationContract]
        List<TableServer> GetServer();
        [OperationContract]
        void PutServerName(string str);

        //Cpu alma fonksiyonları
        [OperationContract]
        List<TableCpu> GetCpu(string str);//herhangi bir sorgulama yapmadan cpu tablosundan bütün değerleri getirir
        [OperationContract]
        List<TableCpu> GetCpu_N(string str, int num);

        //Ram alma fonksiyonları
        [OperationContract]
        List<TableRam> GetRam(string str);//herhangi bir sorgulama yapmadan ram tablosundan bütün değerleri getirir
        [OperationContract]
        List<TableRam> GetRam_N(string str, int num);

        //Drive alma fonksiyonları
        [OperationContract]
        List<TableDrive> GetDrive(string str);//herhang bir sorgulama yapmadan drive tablosundan bütün degerleri alır getirir
        [OperationContract]
        List<TableDrive> GetDrive_N(string str, int num);
        [OperationContract]
        List<TableDrive> GetDriveByName(string str,string drv_name);//DriveName string değerine göre Drive tablosundan sorgulama yapar (Where komutu kullanılır)

        [OperationContract]
        void PutCpuResult(TableCpu obj);
        [OperationContract]
        bool SendMail(string konu, string icerik);
        [OperationContract]
        void PutRamResult(TableRam obj);
        [OperationContract]
        void PutDriveResult(TableDrive obj);
        [OperationContract]
        void Sql_yaz(string com_string); 
    }
}
