﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net.Mail;

namespace WcfService
{
    // NOT: "Service1" sınıf adını kodda, svc'de ve yapılandırma dosyasında birlikte değiştirmek için "Yeniden Düzenle" menüsündeki "Yeniden Adlandır" komutunu kullanabilirsiniz.
    // NOT: Bu hizmeti test etmek üzere WCF Test İstemcisi'ni başlatmak için lütfen Çözüm Gezgini'nde Service1.svc'yi veya Service1.svc.cs'yi seçin ve hata ayıklamaya başlayın.
    public class Service1 : IService1
    {
        public List<TableConf> GetConf()
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var data = from n in obj.TableConfs select n;
            return  data.ToList<TableConf>();
        }

        public void PutConf(TableConf obj)
        {
            string sqlstring = "delete from TableConf";
            Sql_yaz(sqlstring);

            sqlstring = "insert into TableConf(KonfCpu,KonfRam,KonfDisk) " +
                "values ('" + obj.KonfCpu + "','" + obj.KonfRam + "','" + obj.KonfDisk + "')";
            Sql_yaz(sqlstring);
        }

        public bool SendMail(string konu, string icerik)
        {
            MailMessage ePosta = new MailMessage();//göndereceğimiz mailin içeriklerini tutan sınıf
            ePosta.From = new MailAddress("bayrakselim42@gmail.com");//kimden
            ePosta.To.Add("selim161@windowslive.com");//kime
            ePosta.Subject = konu;//mail konusu
            ePosta.Body = icerik;//mail iceriği
            SmtpClient smtp = new SmtpClient();//smtpclient denilen olay mailin gönderiminin yapılacağı mail adresinin bilgilerini alır ve kontrol edip maili yollar.
            smtp.Credentials = new System.Net.NetworkCredential("bayrakselim42@gmail.com", "abuzer1453");//gönderenin şifre ve mail adresi
            smtp.Port = 587;//port adresi 
            smtp.Host = "smtp.gmail.com";//gönderim türünün smtp hostu
            smtp.EnableSsl = true;//güvenlikle alakalı bir konu 
            object userState = ePosta;//gerekli bişey ama bilmiyorum
            bool kontrol = true;//gönderimin kontrolunun yapılması için oluşturulan değişken 
            try
            {
                smtp.SendAsync(ePosta, (object)ePosta);
            }
            catch (SmtpException ex)//burada try catch yapıp hata verdiğinde SmtpExeption tipinde bir hata döndürmem lazım ama gerek yok şuanlık. bu kontrol olayını bool değeriyle yapıyorum zaten
            {
                kontrol = false;//gönderim başarısız
            }
            return kontrol;
        }

        public List<TableServer> GetServer()
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var dataserver = from n in obj.TableServers select n;
            return dataserver.ToList<TableServer>();
        }

        public List<TableCpu> GetCpu(string str)
        {
            //throw new NotImplementedException();
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var dataCpu = obj.TableCpus.Where(x => x.CpuServerName == str);
            return dataCpu.ToList<TableCpu>();
        }

        public List<TableCpu> GetCpu_N(string str, int num)
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var data = obj.TableCpus.Where(x => x.CpuServerName == str).OrderByDescending(x => x.CpuId).Take(num).ToList<TableCpu>();
            return data;
        }

        public List<TableDrive> GetDrive(string str)
        {
            //throw new NotImplementedException();
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var dataDrive = obj.TableDrives.Where(x => x.DriveServerName == str);
            return dataDrive.ToList<TableDrive>();
        }

        public List<TableDrive> GetDrive_N(string str, int num)
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var data = obj.TableDrives.Where(x => x.DriveServerName == str).OrderByDescending(x => x.DrıveId).Take(num).ToList<TableDrive>();
            return data;
        }

        public List<TableDrive> GetDriveByName(string str, string drv_name)
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var data = obj.TableDrives.Where(x => x.DriveName == drv_name && x.DriveServerName==str);
            return data.ToList<TableDrive>();
        }

        public List<TableRam> GetRam(string str)
        {
            //throw new NotImplementedException();
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var dataRam = obj.TableRams.Where(x => x.RamServerName == str);
            return dataRam.ToList<TableRam>();
        }

        public List<TableRam> GetRam_N(string str, int num)
        {
            ServerControlServiceEntities obj = new ServerControlServiceEntities();
            var data = obj.TableRams.Where(x => x.RamServerName == str).OrderByDescending(x => x.RamId).Take(num).ToList<TableRam>();
            return data;
        }

        public void PutServerName(string str)
        {
            string sqlstring = "insert into TableServer(ServerName) " +
                "values ('" + str + "')";
            Sql_yaz(sqlstring);
        }

        public void PutCpuResult(TableCpu obj)
        {
            string sqlstring = "insert into TableCpu(CpuUtility,CpuDate,CpuServerName) " +
                "values ('" + obj.CpuUtility + "','" + obj.CpuDate + "','" + obj.CpuServerName + "')";
            Sql_yaz(sqlstring);

        }

        public void PutRamResult(TableRam obj)
        {
            string sqlstring = "insert into TableRam(RamAvailableSpace,RamDate,RamServerName) " +
                "values ('" + obj.RamAvailableSpace + "','" + obj.RamDate + "','" + obj.RamServerName + "')";
            Sql_yaz(sqlstring);
        }

        public void PutDriveResult(TableDrive obj)
        {
            string sqlstring = "insert into TableDrive(DriveName,DriveType,DriveVolumeLabel,DriveFormat,DriveTotalSize,DriveavailableSize,DriveDate,DriveServerName) " +
                    "values ('" + obj.DriveName + "','" + obj.DriveType + "','" + obj.DriveVolumeLabel + "','" + obj.DriveFormat+ "','" + obj.DriveTotalSize + "','" + obj.DriveAvailableSize + "','" + obj.DriveDate + "','" + obj.DriveServerName + "')";
            Sql_yaz(sqlstring);
        }

        public void Sql_yaz(string com_string)
        {
            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-B2KN1AMO\\SQLEXPRESS;Initial Catalog=ServerControlService;Persist Security Info=True;User ID=test;Password=123");
            conn.Open();
            SqlCommand com = new SqlCommand(com_string, conn);
            com.ExecuteNonQuery();
            conn.Close();
        }
    }
}
