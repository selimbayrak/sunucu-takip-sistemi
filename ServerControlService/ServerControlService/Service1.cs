﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.IO;
using System.Data.SqlClient;
using ServerControlService.WinToWcf;
using System.Net;

namespace ServerControlService
{
    public partial class Service1 : ServiceBase
    {
        
        string servername = Dns.GetHostName();//servername'in adını direk makineden alıyorruz
        public System.Timers.Timer Tmr1;//zamanlayıcı
        
        public Service1()
        {
            //bu kısım olay günlüğünde bir günlük oluşturuyor ve her türlü bilgilendirmeyi oraya ekliyor.
            //windows arama kısmına olay günlüğü yazınca çıkıyor.
            InitializeComponent();
            if (!EventLog.SourceExists("DoDyLogSourse"))
                EventLog.CreateEventSource("DoDyLogSourse", "DoDyLog");
            eventLog1.Source = "DoDyLogSourse";
            eventLog1.Log = "DoDyLog";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Servis Başlatıldı...");//olay günlüğüne ekleme yaparak sistemin çalıştgını belirtiyoruz

            //eger makineden aldıgımız server ismi veritabanında kayıtlı değilsen onun kaydını gerçekleştiriyoruz
            if (!ServernameControl())
            {
                Service1Client obj = new Service1Client();
                obj.PutServerName(servername);
            }

            //timer özelliklerini ayarlıyoruz
            Tmr1 = new System.Timers.Timer();
            Tmr1.Interval = (10000);
            Tmr1.AutoReset = true;
            Tmr1.Enabled = true;
            Tmr1.Start();
            Tmr1.Elapsed += new System.Timers.ElapsedEventHandler(Tmr1_Elapsed);//zamanlayıcı 10 sn sonunda Tmr1_Elapsed fonksiiyonunu tetikliyor.
        }

        protected override void OnStop()
        {
            //servis durdurulduğunda zamanlayıcı sıfırla ve olay günlüğüne durum bildir
            Tmr1.Enabled = false;
            eventLog1.WriteEntry("Servis Durduruldu...");
        }

        private void Tmr1_Elapsed(object sender, EventArgs e)
        {
            Harddisk_gozlem();//disk bilgilerini toparla
            Ram_gozlem();//ram bilgilerini toparla
            Cpu_gozlem();//cpu bilgilerini toparla
        }

        private void Harddisk_gozlem()//harddisk takibi
        {
            //WcfService de oluşturdugumun sınıflardan burada ürettik ve bunların içini doldurup
            //yine WcfService de oluşturdugumuz fonksıyona yollayıp verileri sql db ye yazdırmasını sağladık
            Service1Client obj = new Service1Client();
            TableDrive drive = new TableDrive();
            DriveInfo[] alldriver = DriveInfo.GetDrives();
            foreach (DriveInfo d in alldriver)
            {                
                string temp,temp1;
                temp = "Drive: " + d.Name + "\n"+
                "Drive type: " + d.DriveType;

                eventLog1.WriteEntry(temp);//olay günlüğüne gelen verileri yazdırıp kontrolunu saglıyorum

                drive.DriveName = d.Name;
                drive.DriveType = d.DriveType.ToString();

                if (d.IsReady == true)//eger okunan sistemde birden fazla kayıt diski varsa ve total-available size degerleri sıfır değilse bu kısım çalışır
                {
                    temp1 = "Volume label: " + d.VolumeLabel + "\n" +
                            "File system: " + d.DriveFormat + "\n" +
                            "Total available space: " + d.TotalFreeSpace / (1024 * 1024 * 1024) + " GB\n" +
                            "Total size of drive: " + d.TotalSize / (1024 * 1024 * 1024) + " GB\n";
                    eventLog1.WriteEntry(temp1);//olay günlüğüne yaz

                    drive.DriveVolumeLabel = d.VolumeLabel;
                    drive.DriveFormat = d.DriveFormat;
                    drive.DriveTotalSize = d.TotalSize;
                    drive.DriveAvailableSize = d.TotalFreeSpace;
                    drive.DriveDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");//zaman formatı bazı pc'lerde farklı üretildiği için sorunlar olabiliyor. onu aşmak adına zaman bilgisini biz formatlayıp kaydediyoruz.
                    drive.DriveServerName = servername;
                }
                else//eger disk parcasının total-available size degerleri 0 ise bu kısımda default olarak 0 atanır o degerlere
                {
                    drive.DriveVolumeLabel = null;
                    drive.DriveFormat = null;
                    drive.DriveTotalSize = 0;
                    drive.DriveAvailableSize = 0;
                    drive.DriveDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    drive.DriveServerName = servername;

                }
                obj.PutDriveResult(drive);//toparlanan bilgileri wcf'e yolla veritabanına yazsın
            }
        }

        private void Ram_gozlem()//ram takibi
        {
            Service1Client obj = new Service1Client();//wcf servis türünde bir nesne
            TableRam ram = new TableRam();

            PerformanceCounter ramCounter;//ram ullanımını ölçmek için oluşturdugumuz nesne
            ramCounter = new PerformanceCounter("Memory", "Available MBytes", true);            
            eventLog1.WriteEntry("Available Space Ram: "+ ramCounter.NextValue() +" MB");
            ram.RamAvailableSpace = ramCounter.NextValue();
            ram.RamDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            ram.RamServerName = servername;

            obj.PutRamResult(ram);//sonucları veritabanına wcf aracılıgıyla yazdır
        }

        //1 sn aralıklarla cpu nun kullanımını ölçüp cıkan degeri veritabanına kaydediyoruz
        public void Cpu_gozlem()//cpu takibi
        {
            Service1Client obj = new Service1Client();
            TableCpu cpu = new TableCpu();

            PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            cpuCounter.NextValue();
            CounterSample c1 = cpuCounter.NextSample();
            Thread.Sleep(1000);
            CounterSample c2 = cpuCounter.NextSample();

            eventLog1.WriteEntry("CPU: " + CounterSample.Calculate(c1, c2) + " %");

            cpu.CpuUtility = CounterSample.Calculate(c1, c2).ToString(); 
            cpu.CpuDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            cpu.CpuServerName = servername;

            obj.PutCpuResult(cpu);
        }

        //server'dan aldıgımız servername adlı degerin veritabanında kayıtlı olup olmadıgını kontrol ediyor
        public bool ServernameControl()
        {
            Service1Client obj = new Service1Client();
            string str = Dns.GetHostName();
            var data = obj.GetServer();
            bool IsRecorded = false;
            foreach (var item in data)
            {
                if(item.ServerName == str)
                {
                    IsRecorded = true;
                    return IsRecorded;
                }
            }
            return IsRecorded;
        }
    }
}